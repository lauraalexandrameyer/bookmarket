package net.tncy.mey.bookmarket.data;

public class InventoryEntry {
    
    private Book book;
    private Integer quantity;
    private Float averagePrice;
    private Float currentPrice;
    
    public InventoryEntry(Book book, Integer quantity, Float averagePrice, Float currentPrice){
        this.book = book;
        this.quantity = quantity;
        this.averagePrice = averagePrice;
        this.currentPrice = currentPrice;
    }
    
}